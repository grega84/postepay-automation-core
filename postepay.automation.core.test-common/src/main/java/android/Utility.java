package android;


import static java.lang.Math.toIntExact;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;


public class Utility {
	public enum DIRECTION {
	    DOWN, UP, LEFT, RIGHT;
	}
	public static void swipeFromBorders(MobileDriver driver, DIRECTION direction, long duration) {
	    Dimension size = driver.manage().window().getSize();

	    int startX = 0;
	    int endX = 0;
	    int startY = 0;
	    int endY = 0;

	    switch (direction) {
	        case RIGHT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width);
	            endX = (int) (size.width * 0.05);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(endX, startY)
	                    .release()
	                    .perform();
	            break;

	        case LEFT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width * 0.05);
	            endX = (int) (size.width);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(endX, startY)
	                    .release()
	                    .perform();

	            break;

	        case UP:
	            endY = (int) (size.height * 0.70);
	            startY = (int) (size.height);
	            startX = (size.width / 2);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(endX, startY)
	                    .release()
	                    .perform();
	            break;


	        case DOWN:
	            startY = (int) (size.height);
	            endY = (int) (size.height * 0.30);
	            startX = (size.width / 2);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(startX, endY)
	                    .release()
	                    .perform();

	            break;

	    }
	}
	
	public static void swipe(MobileDriver driver, DIRECTION direction, long duration) {
	    Dimension size = driver.manage().window().getSize();

	    int startX = 0;
	    int endX = 0;
	    int startY = 0;
	    int endY = 0;

	    switch (direction) {
	        case RIGHT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width * 0.90);
	            endX = (int) (size.width * 0.05);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(endX, startY)
	                    .release()
	                    .perform();
	            break;

	        case LEFT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width * 0.05);
	            endX = (int) (size.width * 0.90);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(endX, startY)
	                    .release()
	                    .perform();

	            break;

	        case UP:
	            endY = (int) (size.height * 0.70);
	            startY = (int) (size.height * 0.30);
	            startX = (size.width / 2);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(endX, startY)
	                    .release()
	                    .perform();
	            break;


	        case DOWN:
	            startY = (int) (size.height * 0.70);
	            endY = (int) (size.height * 0.30);
	            startX = (size.width / 2);
	            new TouchAction(driver)
	                    .press(startX, startY)
	                    .waitAction(toIntExact(duration))
	                    .moveTo(startX, endY)
	                    .release()
	                    .perform();

	            break;

	    }
	}
	public static File takePartOfScreenByElement(AppiumDriver driver,MobileElement element) throws IOException{
//		driver.get("http://www.google.com");
//		WebElement ele = driver.findElement(By.id("hplogo"));

		// Get entire page screenshot
		File screenshot = ((TakesScreenshot)driver).getScreenshotAs(org.openqa.selenium.OutputType.FILE);
		BufferedImage  fullImg = ImageIO.read(screenshot);

		// Get the location of element on the page
		org.openqa.selenium.Point point = element.getLocation();

		// Get width and height of the element
		int eleWidth = element.getSize().getWidth();
		int eleHeight = element.getSize().getHeight();

		// Crop the entire page screenshot to get only element screenshot
		BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(),
		    eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);

		// Copy the element screenshot to disk
		File screenshotLocation = File.createTempFile("part_screen", "png");
		org.apache.commons.io.FileUtils.copyFile(screenshot, screenshotLocation);
		return screenshotLocation;
	}
	
	public static void cmpImage(File img1,File img2){
		Mat mimg1=Imgcodecs.imread(img1.getAbsolutePath());
		Mat mimg2=Imgcodecs.imread(img2.getAbsolutePath());

		double l2_norm = Core.norm(mimg1, mimg2);
		System.out.println("--------"+l2_norm);
	}
}

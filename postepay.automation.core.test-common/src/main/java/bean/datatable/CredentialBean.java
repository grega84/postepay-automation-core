package bean.datatable;

public class CredentialBean {
private String userName;
private String password;
private String email;
private String posteId;

public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPosteId() {
	return posteId;
}
public void setPosteId(String posteId) {
	this.posteId = posteId;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

}

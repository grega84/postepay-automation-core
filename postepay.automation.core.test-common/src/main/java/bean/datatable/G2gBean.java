package bean.datatable;

public class G2gBean {
	private String recipient;
	private String posteId;
	private String requested;
	
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getRequested() {
		return requested;
	}
	public void setRequested(String requested) {
		this.requested = requested;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCheckedStatus() {
		return checkedStatus;
	}
	public void setCheckedStatus(String checkedStatus) {
		this.checkedStatus = checkedStatus;
	}
	public String getContactAlreadyExists() {
		return contactAlreadyExists;
	}
	public void setContactAlreadyExists(String contactAlreadyExists) {
		this.contactAlreadyExists = contactAlreadyExists;
	}
	private String description;
	private String checkedStatus;
	private String contactAlreadyExists;
	
	public String getPosteId() {
		return posteId;
	}
	public void setPosteId(String posteId) {
		this.posteId = posteId;
	}
	
}

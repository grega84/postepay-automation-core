package bean.datatable;

public class P2pBean {
	private String amount;
	private String recipient;
	private String requested;
	private String description;
	private String posteId;
	private String checkedStatus;
	private String contactAlreadyExists;
	
	public String getRequested() {
		return requested;
	}
	public void setRequested(String requested) {
		this.requested = requested;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPosteId() {
		return posteId;
	}
	public void setPosteId(String posteId) {
		this.posteId = posteId;
	}
	public String getCheckedStatus() {
		return checkedStatus;
	}
	public void setCheckedStatus(String checkedStatus) {
		this.checkedStatus = checkedStatus;
	}
	public String getContactAlreadyExists() {
		return contactAlreadyExists;
	}
	public void setContactAlreadyExists(String contactAlreadyExists) {
		this.contactAlreadyExists = contactAlreadyExists;
	}
}

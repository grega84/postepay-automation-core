package bean.datatable;

public class PostagiroBean {
	private String iban;
	private String destinationName;
	private String amount;
	private String description;
	private String posteId;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPosteId() {
		return posteId;
	}
	public void setPosteId(String posteId) {
		this.posteId = posteId;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}

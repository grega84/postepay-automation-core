package bean.datatable;

public class RechargePhoneBean {
	private String phoneNumber;
	private String phoneOperator;
	private String amount;
	private String posteId;
	public String getPosteId() {
		return posteId;
	}
	public void setPosteId(String posteId) {
		this.posteId = posteId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneOperator() {
		return phoneOperator;
	}
	public void setPhoneOperator(String phoneOperator) {
		this.phoneOperator = phoneOperator;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	

}

package bean.datatable;

public class RechargePostepayBean {
	private String cardNumber;
	private String destinationName;
	private String amount;
	private String posteId;
	public String getPosteId() {
		return posteId;
	}
	public void setPosteId(String posteId) {
		this.posteId = posteId;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	

}

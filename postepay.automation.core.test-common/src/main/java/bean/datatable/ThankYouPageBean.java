package bean.datatable;

import org.apache.commons.lang3.StringUtils;

import junit.framework.Assert;

public class ThankYouPageBean {
	private String transactionMessage;
	private String operationText;
	public String getTransactionMessage() {
		return transactionMessage;
	}
	public void setTransactionMessage(String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}
	public String getOperationText() {
		return operationText;
	}
	public void setOperationText(String operationText) {
		this.operationText = operationText;
	}
	
	
}

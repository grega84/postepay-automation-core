//package general;
//import java.awt.image.BufferedImage;
//import java.io.ByteArrayInputStream;
//import java.io.File;
//import java.io.IOException;
//import java.util.Base64;
//
//import javax.imageio.ImageIO;
//
//import org.openqa.selenium.Dimension;
//import org.openqa.selenium.OutputType;
//import org.sikuli.script.Finder;
//import org.sikuli.script.Location;
//import org.sikuli.script.Match;
//import org.sikuli.script.Pattern;
//
//import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.MobileElement;
//import io.appium.java_client.TouchAction;
//import io.appium.java_client.touch.offset.PointOption;
//
//public class ImageUtils 
//{
//	public static void FindAndClick(AppiumDriver<MobileElement> driver,String find,float similarity) throws Exception
//	{
//		BufferedImage image = null;
//		byte[] imageByte;
//
//		Base64.Decoder decoder = Base64.getDecoder();
//		imageByte = decoder.decode(driver.getScreenshotAs(OutputType.BASE64));
//		ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
//		image = ImageIO.read(bis);
//		bis.close();
//		
//		ImageIO.write(image, "png", new File("finder.png"));
//		
//		Finder src = new Finder("finder.png");
//	    Pattern pat = new Pattern(find).similar(similarity);
//	    src.find(pat);
//	    Match m=null;
//	    
//	    while( src.hasNext()) 
//	    {
//	    	m = src.next();
//	    }
//	    
//	    if(m == null)
//	    	throw new Exception("Image not found "+find);
//	    
//	    src.destroy();
//	    
//	    Location l=m.getCenter();
//	    
//	    BufferedImage I=ImageIO.read(new File("finder.png"));
//	    
//	    float w,h;
//	    
//	    w=I.getWidth();
//	    h=I.getHeight();
//	    
//	    float dx,dy;
//	    
//	    dx=l.x / w;
//	    dy=l.y / h;
//	    
//	    Dimension windowSize = driver.manage().window().getSize();
//	    
//	    TouchAction action= new TouchAction(driver);
//	    action.press(PointOption.point((int)(dx * windowSize.width), (int)(dy * windowSize.height))).perform();
//	}
//}

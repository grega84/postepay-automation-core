package android;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;

import scala.annotation.meta.param;

public class Locator 
{
	public interface ILocator
	{
		public String getLocator(String driverType);
	}
	
	public enum Login implements ILocator
	{
	    TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']",
	    		"//*[@accessibilityLabel='Accedi']"),
	    POSTE_IT_TAB("//android.widget.LinearLayout/android.widget.TextView[@text='POSTE.IT']",
	    		"//*[@text='POSTE.IT']"),
	    POSTEID_TAB("//android.widget.LinearLayout/android.widget.TextView[@text='POSTEID']",
	    		"//*[@text='POSTEID']"),
	    FIELD_USER_POSTE_IT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_user_name']",
	    		"//*[@class='UIAScrollView'][1]//*[@placeholder='Nome utente']"),
	    FIELD_USER_POSTEID("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_user_email']",
	    		"//*[@class='UIAScrollView'][2]//*[@placeholder='Indirizzo email']"),
	    FIELD_PASSWORD("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_password']",
	    		"//*[@class='UIAScrollView'][1]//*[@placeholder='Password']"),
	    FIELD_PASSWORD_POSTEID("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_password']",
	    		"//*[@class='UIAScrollView'][2]//*[@placeholder='Password']"),
	    ACCEDI_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_sign_in']",
	    		"//*[@class='UIAScrollView'][1]//*[@text='ACCEDI']"),
	    ACCEDI_BTN_POSTEID("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_sign_in']",
	    		"//*[@class='UIAScrollView'][2]//*[@text='ACCEDI']"),
		REGISTRATION_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_register']",
				"//*[@class='UIAScrollView'][1]//*[@text='REGISTRATI']"),
		REGISTRATION_BTN_POSTEID("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_register']",
				"//*[@class='UIAScrollView'][2]//*[@text='REGISTRATI']"),
		LOCATOR_TAB_SELECTED("//android.widget.LinearLayout/android.widget.TextView[@selected='true']",
				"//*[@enabled='false' and @XCElementType='XCUIElementTypeButton' and contains(@text,'POSTE')]");

		
		//	    TESTTTTTT("${Prova}-----");
		
	    private String locator;
	    private String locatorIOS;

	    Login(String locator, String locatorIOS) {
	        this.locator = locator;
	        this.locatorIOS=locatorIOS;
	    }
	    
	    public String getLocator(String driverType) 
	    {
	    	switch(driverType)
	    	{
	    	case "ios":
	    		return locatorIOS;
	    		default:
	    			return locator;
	    	}
	        //return locator;
	    }
//	    public String generateLocator(Map<String, String> parameters){
//	    	StrSubstitutor substitutor= new StrSubstitutor(parameters);
//	    	return substitutor.replace(locator);
//	    }
	}
	
	
	public static void main(String[] args) {
		Map<String, String> test= new HashMap<>();
		test.put("Prova", "primaprova");
//		System.out.println(Login.TESTTTTTT.generateLocator(test));
	}
	
	public enum CustomizationPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		ACCEPT_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/access_bt']"),
		DENY_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/second_bt']");
		 private String locator;

		 CustomizationPage(String locator) {
		        this.locator = locator;
		    }
		    
		    public String getLocator() {
		        return locator;
		    }

		
		

	}
	
	
	public enum TutorialP2P{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/toolbar_title']"),
		CONFIGURA_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/tutorial_p2p_bt']"),
		CLOSE_BTN("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/toolbar_close']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		TutorialP2P(String locator) {
			this.locator=locator;
		}
	}
	
	public enum InserisciPosteID{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		CODICEPOSTEIDFIELD("//android.widget.LinearLayout[@resource-id='posteitaliane.posteapp.apppostepay:id/ptl_posteid']"),
		CODICEPOSTEIDINSERTFIELD("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_poste_id']"),
		NONSEITU("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/non_sei_tu']"),
		CONFERMA_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm_poste_id']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		InserisciPosteID(String locator) {
			this.locator=locator;
		}
	}
	
	public enum ThankYouPage_2{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_transaction_header']"),
		CLOSE_BTN("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_close']"),
		NO_GRAZIE_BUTTON("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_buttonDefaultPositive']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		ThankYouPage_2(String locator) {
			this.locator=locator;
		}
	}
	
	public enum AssistenzaPage{
		LOGO_ASSISTENZA("//android.widget.ImageButton[@resource-id='posteitaliane.posteapp.apppostepay:id/ib_right']"),
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		DOMANDE_FREQUENTI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/textView18']"),
		ACCESSO_APP("//*[@text='ACCESSO ALL�APP']"),
		OPERAZIONI_IN_APP("//*[@text='OPERAZIONI IN APP']"),
		LE_MIE_CARTE_POSTEPAY("//*[@text='LE MIE CARTE POSTEPAY']"),
		POSTEPAY2POSTEPAY("//*[@text='POSTEPAY2POSTEPAY']"),
		PAGA_CON_POSTEPAY("//*[@text='PAGA CON POSTEPAY']"),
		SCONTI_BANCOPOSTA("//*[@text='SCONTI BANCOPOSTA']"),
		SICUREZZA("//*[@text='SICUREZZA']"),
		TERMINI_E_CONDIZIONI("//*[@text='TERMINI E CONDIZIONI']"),
		POSTEPAY_CONNECT("//*[@text='POSTEPAY CONNECT']"),
		BLOCCA_CARTE("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_chiama_3']"),
		CHIAMACI_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_chiama']"),
		AIUTO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/textView17']"),
		NON_HAI_TROVATO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/textView30']"),
		CHATTA_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_chiama_2']"),
		BANCOPOSTA("//*[@text='BancoPosta']"),
		UFFICIO_POSTALE("//*[@text='Ufficio Postale']"),
		NUMERO_TELEFONO("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_chat_telefono']"),
		REQUESTED("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_chat_message']"),
		RISPOSTA("//*[@text='Ti stiamo mettendo in contatto con un operatore']"),
		AVVISO("posteitaliane.posteapp.apppostepay:id/md_title"),
		AVVISO_OK("posteitaliane.posteapp.apppostepay:id/md_buttonDefaultPositive"),
		PRENOTA_BUTTON ("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_chat_action']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		AssistenzaPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum ThankYouPage_G2G{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_transaction_header']"),
		CLOSE_BTN("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_close_bollettino']"),
		NO_GRAZIE_BUTTON("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_buttonDefaultPositive']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		ThankYouPage_G2G(String locator) {
			this.locator=locator;
		}
	}
	
	public enum P2PPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/toolbar_title']"),
		AMOUNT_TXT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_amount']"),
		CLOSE_BTN("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/toolbar_close']"),
		EURO_SYMBOL("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_currency']"),
		SEND_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_p2p_send']"),
		DISCLAIMER_TXT("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_amount_disclaimer']"),
		REQUEST_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_p2p_receive']");
		
		private String locator;
		public String getLocator(){
			return locator;
		}
		P2PPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum G2GPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/toolbar_title']"),
	    GB_DISPONIBILI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_disposizione']"),
	    GB_UTILIZZATI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_usato']"),
		G2G_REGALA("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_regala']"),
		G2G_ACQUISTA("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_acquista']"),
		G2G_SEE_DETAILS("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_dettagli']"),
		G2G_UPDATE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_update']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		G2GPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum G2GRegalaPage{
		TEXT_DESCRIPTION("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		PROCEDI_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_procedi']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		G2GRegalaPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum G2GAquistaPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		CORRETTO("//android.widget.TextView[@text='� tutto corretto?']"),
		DESCRIZIONE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_desc']"),
		GIGA_QUANTITY("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_cifra_giga']"),
		GIGA_SCADENZA("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_scadenza']"),
		PAGA_CON_PRODOTTO("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_pagacon']"),
		IMPORTO("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_importo']"),
		INVIA_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_invia']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		G2GAquistaPage(String locator) {
			this.locator=locator;
		}
	}
	
	
	public enum G2GDetailsGbPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
	    GB_DISPONIBILI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_disponibile']"),
	    LABEL_GB_PIANO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title_piano']"),
	    LABEL_GB_ACQUISTATI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title_acquistati']"),
	    LABEL_GB_RICEVUTI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title_g2g']"),
	    NUMBER_GB_PIANO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_giga_piano']"),
	    NUMBER_GB_ACQUISTATI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_giga_acquistati']"),
	    NUMBER_GB_RICEVUTI("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_giga_g2g']"),
	    ICON_UPDATE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_update]"),
	    GB_BAR("//android.widget.ProgressBar[@resource-id='posteitaliane.posteapp.apppostepay:id/pb_amount']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		G2GDetailsGbPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum G2GPageCompile{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		GIGA_QUANTITY("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_giga']"),
		DESTINATION("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_destinatario']"),
		PHONE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_phone_number']"),
		NEXT_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_send_to']"),
		AVVISO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_title']"),
		DESCRIZIONE_AVVISO("//android.widget.TextView[@text=\"L'utente selezionato non ha Postepay Connect.\"]"),
		OK_BUTTON("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_buttonDefaultPositive']"),
		SINGLE_CONTACT("//android.widget.ListView[@resource-id='posteitaliane.posteapp.apppostepay:id/list']//android.widget.TextView[@text='${contactName}']"),
		LOGO("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/iv_pp_logo']");

		private String locator;

		public String getLocator(){
			return locator;
		}
		public String getLocatorContact(String contactName){
			Map<String, String> parameter= new HashMap<>();
			parameter.put("contactName", contactName);
			StrSubstitutor substitutor= new StrSubstitutor(parameter);
	    	return substitutor.replace(locator);
		}
		
		G2GPageCompile(String locator) {
			this.locator=locator;
		}
	}
	
	public enum G2GRiepilogoPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		GIGA_QUANTITY("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_giga']"),
		DESTINATION_CONTACT_TO_SEND("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_destinatario']"),
		EDIT_DESTINATION("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/iv_edit_destinatario']"),
		INVIA_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btn_invia']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		G2GRiepilogoPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum G2GPosteIDPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		POSTE_ID("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/et_poste_id']"),
		CONFERMA_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm_poste_id']");
		private String locator;
		public String getLocator(){
			return locator;
		}
		G2GPosteIDPage(String locator) {
			this.locator=locator;
		}
	}
	
//	public enum CardData{
//		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/toolbar_title']"),
//		CC_CARD_IMAGE("//android.widget.FrameLayout[@resource-id='posteitaliane.posteapp.apppostepay:id/cc_card']"),
//		CC_CARD_NUMBER("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_card_number']"),
//		CC_EXPIRE_DATE("//android.widget.TextView(@resource-id='posteitaliane.posteapp.apppostepay:id/expire_date']"),
//		NEXT_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_next']"),
//		MONTH_SWIPED_OBJECT("//android.view.View[@resource-id='posteitaliane.posteapp.apppostepay:id/options1']"),
//		YEAR_SWIPED_OBJECT("//android.view.View[@resource-id='posteitaliane.posteapp.apppostepay:id/options2']"),
//		CANCEL_EXPIRED_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btnCancel']"),
//		SUBMIT_EXPIRED_BUTTON("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btnSubmit']"),
//	}
	
	
	public enum P2PInviaPage{
		TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
		CONTACT_SEARCH("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_send_to']"),
		CONTACT_TAB("//android.widget.TextView[@text='CONTATTI']"),
		CONTACTP2P_TAB("//android.widget.TextView[@text='CONTATTI P2P']"),
		NEXT_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_send_to']"),
		CONFIRM_DESCRIPTION_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_causal_confirm']"),
		MODIFY_DESTINATION_BTN("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/iv_send_to_collapse']"),
		CONTACT_LIST("//android.widget.ListView[@resource-id='posteitaliane.posteapp.apppostepay:id/swipe_target']"),
		DESTINATION_MESSAGE("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_casual']"),
		SEND_PAYMENT("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm_payment']"),
		DESTINATION_CONTACT_TO_SEND("//android.widget.TextView[@text='A chi']/following-sibling::android.widget.TextView"),
		SINGLE_CONTACT("//android.widget.ListView[@resource-id='posteitaliane.posteapp.apppostepay:id/swipe_target']//android.widget.TextView[@text='${contactName}']");
		
		private String locator;

		public String getLocator(){
			return locator;
		}
		public String getLocatorContact(String contactName){
			Map<String, String> parameter= new HashMap<>();
			parameter.put("contactName", contactName);
			StrSubstitutor substitutor= new StrSubstitutor(parameter);
	    	return substitutor.replace(locator);
		}
		
		P2PInviaPage(String locator) {
			this.locator=locator;
		}
	}
	public enum ThankYouPage{
		TRANSACTION_MESSAGE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_transaction_header']"),
		DESCRIPTION_MESSAGE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_transaction_description']"),
		TRY_AGAIN_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_try_again']"),
		CLOSE_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_close_2'");
		private String locator;

		public String getLocator(){
			return locator;
		}
		private ThankYouPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum PopUpAlert{
		TITLE_POPUP("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_title']"),
		DESCRIPTION_POPUP("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_content']"),
		CONFIRM_BTN("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_content']");
		private String locator;

		public String getLocator(){
			return locator;
		}
		private PopUpAlert(String locator) {
			this.locator=locator;
		}
	}
	
	public enum MoneyTransferThankYouPage{
		TRANSACTION_MESSAGE("//android.widget.TextView[@resource-id='//posteitaliane.posteapp.apppostepay:id/tv_transaction_header']"),
		DESCRIPTION("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_transaction_description']"),
		CLOSE_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/relativeLayout8']");
		private String locator;

		public String getLocator(){
			return locator;
		}
		private MoneyTransferThankYouPage(String locator) {
			this.locator=locator;
		}
	}
	
	public enum OperationCompleted{
		TEXT_OPERATION("//android.widget.TextView[contains(@text,'eseguita')]"),
		CLOSE_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_close']"); 
		private String locator;

		public String getLocator(){
			return locator;
		}
		private OperationCompleted(String locator){
			this.locator=locator;
		}
	}
	
	
	public enum PosteIdCodeRequiredPage{
		HEADER_DESCRIPTION("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title_header']"),
		POSTEID_FIELD_TEXT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_poste_id']"),
		CONFIRM_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm_poste_id']");
		private String locator;

		public String getLocator(){
			return locator;
		}
		
		PosteIdCodeRequiredPage(String locator){
			this.locator=locator;
		}
	}
	
	
	
	public enum Homepage {
	    TOOLBAR("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
	    TITLE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
//	    CARD_CONTAINER_FOCUS("//android.widget.RelativeLayout[@resource-id='posteitaliane.posteapp.apppostepay:id/info_container']"),
	    CARD_TYPE_IMAGE("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/creditcard_type']"),
	    AMOUNT_CARD_VALUE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/balance_text']"),
	    ARROW_ICON("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/arrow']"),
	    CONTAINER_G2G("//android.view.ViewGroup[@resource-id='posteitaliane.posteapp.apppostepay:id/card_sim_connect']"),
	    ICON_G2G("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/icona']");
	    private String locator;

	    Homepage(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	
	public enum TutorialNotifiche {
	    TITLENOTIFICHE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
	    ATTIVANOTIFICHE("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/access_bt']"),
	    BACKROWNOTIFICHE("//android.widget.ImageButton[@resource-id='posteitaliane.posteapp.apppostepay:id/ib_left']");
	    private String locator;

	    TutorialNotifiche(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	
	public enum TutorialPersonalizzazione {
	    TITLEPERSONALIZZAZIONE("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
	    ACCETTOPERSONALIZZIONE("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/access_bt']");
	    private String locator;

	    TutorialPersonalizzazione(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	
	public enum TutorialConnect {
	    TITLECONNECT("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/toolbar_title']"),
	    SALTABUTTON("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_salta']");
	    private String locator;

	    TutorialConnect(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	
	public enum TutorialGPay {
	    TITLEGPAY("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
	    CHECKBOXNONMOSTRAREPIU("//android.widget.CheckBox[@resource-id='posteitaliane.posteapp.apppostepay:id/cb_ricorda']"),
	    XCHIUSURA("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/iv_close']");
	    private String locator;

	    TutorialGPay(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	
	public enum ReciveP2pSendBar {
//	    ALL_BAR_VISIBLE("//android.widget.LinearLayout[@resource-id='posteitaliane.posteapp.apppostepay:id/container_collapsed']"),
	    RECEIVE_BTN("//android.widget.TextView[@text='RICEVI']"),
	    P2P_BTN("//android.widget.ImageButton[@resource-id='posteitaliane.posteapp.apppostepay:id/fab']"),
	    SEND_BTN("//android.widget.TextView[@text='PAGA']");
	    private String locator;

	    ReciveP2pSendBar(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	
	public enum SearchAndFilter {
	    SEARCH_IMG("//android.widget.ImageView[@resource-id='posteitaliane.posteapp.apppostepay:id/imageView3']"),
	    SEARCH_FIELD("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/search_filter_text']"),
	    FILTER_LABEL("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_filter_text']"),
	    FILTER_COUNTER_LABEL("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_filter_counter']");
	    private String locator;

	    SearchAndFilter(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	public enum ItemDetails{
		DESCRIPTION("description"),
		PAYMENT("payment"),
		PAYMENT_DATE("paymentDate");
		private String value;
		ItemDetails(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
		
	}
	public enum MovementList {
	    MOVEMENT_LIST_AREA_VISIBLE("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/swipe_target']");

	    private String locator;
	    private final String SINGLE_ITEM= "//*[@resource-id='posteitaliane.posteapp.apppostepay:id/swipe_target']/android.view.ViewGroup[${itemNumber}]";
	    private final String SINGLE_ITEM_DETAILS_DESCRIPTION="//android.widget.TextView[contains(@resource-id,'posteitaliane.posteapp.apppostepay:id/tv_header1')]";
	  //*[@resource-id='posteitaliane.posteapp.apppostepay:id/swipe_target']/android.widget.RelativeLayout[1]//android.widget.TextView[contains(@resource-id,'description')]
	    private final String SINGLE_ITEM_DETAILS_PAYMENT="//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_amount']";
	    private final String SINGLE_ITEM_DETAILS_PAYMENT_DATE="//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_date']";

	    MovementList(String locator) {
	        this.locator = locator;
	    }
	    public String getSingleItem(String itemNumber){
	    	Map<String, String> parameters = new HashMap<>();
	    	parameters.put("itemNumber", itemNumber);
	    	StrSubstitutor substitutor= new StrSubstitutor(parameters);
	    	return substitutor.replace(SINGLE_ITEM);
	    }
	    public String getItemDetailsLocator(String itemNumber,ItemDetails typeDetails){
	    	String singleItem= getSingleItem(itemNumber);
	    	StringBuilder itemToSelect= new StringBuilder(singleItem);
	    	switch (typeDetails.getValue()) {
			case "description":
				itemToSelect.append(SINGLE_ITEM_DETAILS_DESCRIPTION);
				break;
			case "payment":
				itemToSelect.append(SINGLE_ITEM_DETAILS_PAYMENT);
				break;
			case "paymentDate":
				itemToSelect.append(SINGLE_ITEM_DETAILS_PAYMENT_DATE);
				break;
			default:
				break;
			}
	    	return itemToSelect.toString();
	    }
	    
	    public String getLocator() {
	    	return locator;
	    }
	}
	
	
	
	
	public enum CardDetails {
	    CARD_SELECTED("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/swipe_target']"),
	    HOW_TO_USE_AREA("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/tv_title']"),
	    BALANCE_AREA("//android.widget.LinearLayout/android.widget.TextView[@text='POSTE.IT'}"),
	    AVAILABLE_BALANCE_LABEL("//android.widget.LinearLayout/android.widget.TextView[@text='POSTEID'}"),
	    GOOGLE_PAY_AREA(""),
	    GOOGLE_PAY_LOGO(""),
	    GOOGLE_PAY_TOGGLE(""),
	    APP_OPERATION_AREA(""),
	    ENABLE_CARD_IN_APP_BTN(""),
	    IBAN_AREA_(""),
	    IBAN_LABEL(""),
	    SHARE_IBAN(""),
	    CARD_ALIAS_AREA(""),
	    CARD_ALIAS_VALUE_LABEL(""),
	    CARD_ALIAS_MODIFY_IMAGE(""),
	    BANCOPOSTA_DISCOUNT_AREA(""),
	    MY_BANCOPOSTA_DISCOUNT_BTN(""),
	    
	    
	    ACCOUNTING_BALANCE_LABEL("//android.widget.LinearLayout/android.widget.TextView[@text='POSTEID'}");
		
		
		private String locator;

		CardDetails(String locator) {
	        this.locator = locator;
	    }

	    public String getLocator() {
	        return locator;
	    }
	}
	public enum Tutorial{
		TRAINING_LOGO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/iv_logo_intro']"),
		GO_TO_LOGIN_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/access_bt']");		
		private String locator;
		
		Tutorial(String locator){
			this.locator=locator;
		}
		public String getLocator() {
	        return locator;
	    }
	}
	public enum PopUpSendReceive{
		TITLE_POPUP("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/text_title_popup_window']"),
		SCROLLABLE_LIST("//android.support.v7.widget.RecyclerView[@resource-id='posteitaliane.posteapp.apppostepay:id/recycler_actions']");
		private String locator;
		public String getLocator() {
	        return locator;
	    }
		
		private final static String SINGLE_ITEM=SCROLLABLE_LIST.getLocator()+"//android.widget.TextView[@text='${operationName}']";
		PopUpSendReceive(String locator){
			this.locator=locator;
		}
		
		public String getSingleItem(String itemToSelect){
			Map<String, String> parameters = new HashMap<>();
	    	parameters.put("operationName", itemToSelect);
	    	StrSubstitutor substitutor= new StrSubstitutor(parameters);
	    	return substitutor.replace(SINGLE_ITEM);
		}
		
	}
	
	public enum BonificoSEPA{
		CONFIRM_DESTINATION_COUNTRY("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/btnSubmit']"),
		IBAN_FIELD_TXT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_iban']"),
		DESTINATION_NAME("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_intestatario']"),
		DESTINATION_COUNTRY("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_paese_residenza']"),
		AMOUNT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_amount']"),
		NEXT_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm']"),
		DESCRIPTION_TXT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_message']"),
		POPUP_AVVISO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_title']"),
		BUTTON_OK("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_buttonDefaultPositive']");
		private String locator;
		public String getLocator() {
	        return locator;
	    }
		BonificoSEPA(String locator){
			this.locator=locator;
		}
		
		
	}
	public enum Postagiro{
		IBAN_FIELD_TXT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_iban']"),
		DESTINATION_NAME("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_intestatario']"),
		AMOUNT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_amount']"),
		NEXT_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm']"),
		DESCRIPTION_TXT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_message']"),
		POPUP_AVVISO("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_title']"),
		BUTTON_OK("//android.widget.TextView[@resource-id='posteitaliane.posteapp.apppostepay:id/md_buttonDefaultPositive']");
		private String locator;
		public String getLocator() {
	        return locator;
	    }
		Postagiro(String locator){
			this.locator=locator;
		}
		
		
	}
	public enum RechargePostepay{
		CARD_NUMBER("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_pan']"),
		DESTINATION_NAME("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_intestatario']"),
		AMOUNT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_amount']"),
		PAY_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm']"),
		NEXT_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm']"),
		DESCRIPTION_FIELD("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_causale']");
		private String locator;
		public String getLocator() {
	        return locator;
	    }
		RechargePostepay(String locator){
			this.locator=locator;
		}
	}
	public enum RechargePhone{
		MOBILE_NUMBER("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_telephone_number']"),
		OPERATOR("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_mobile_provider']"),
		AMOUNT("//android.widget.EditText[@resource-id='posteitaliane.posteapp.apppostepay:id/et_amount']"),
		PAY_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm']"),
		NEXT_BTN("//android.widget.Button[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_confirm']");
		private String locator;
		public String getLocator() {
	        return locator;
	    }
		RechargePhone(String locator){
			this.locator=locator;
		}
	}
	
	
}

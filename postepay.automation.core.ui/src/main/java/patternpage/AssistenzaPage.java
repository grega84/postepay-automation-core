package patternpage;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import android.Utility;
import bean.datatable.AssistenzaBean;
import bean.datatable.P2pBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class AssistenzaPage extends LoadableComponent<AssistenzaPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement logo_assistenza=null;
	private MobileElement title=null;
	private MobileElement domandefrequenti=null;
	private MobileElement accessoapp=null;
	private MobileElement operazioniapp=null;
	private MobileElement lemiecartepp=null;
	private MobileElement p2p=null;
	private MobileElement pagaconpp=null;
	private MobileElement scontibancoposta=null;
	private MobileElement sicurezza=null;
	private MobileElement ppconnect=null;
	private MobileElement terminiecondizioni=null;
	private MobileElement bloccacarte=null;
	private MobileElement aiuto=null;
	private MobileElement chiamacibutton=null;
	private MobileElement nonhaitrovato=null;
	private MobileElement chattabutton=null;
	private MobileElement bancopostaapp=null;
	private MobileElement ufficiopostaleapp=null;
	private MobileElement numerotelefono=null;
	private MobileElement prenotabutton=null;
	private MobileElement requested=null;
	private MobileElement risposta=null;
	private MobileElement avviso=null;
	private MobileElement avviso_ok=null;

	public AssistenzaPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		logo_assistenza=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.LOGO_ASSISTENZA.getLocator())),40);
		Assert.assertTrue(logo_assistenza!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public void openAssistenza() {
		logo_assistenza.click();
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.TITLE.getLocator())),40);
		domandefrequenti=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.DOMANDE_FREQUENTI.getLocator())),40);
		accessoapp=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.ACCESSO_APP.getLocator())),40);
		operazioniapp=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.OPERAZIONI_IN_APP.getLocator())),40);
		lemiecartepp=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.LE_MIE_CARTE_POSTEPAY.getLocator())),40);
		Utility.swipe(driver, Utility.DIRECTION.DOWN,1000);
		
		p2p=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.POSTEPAY2POSTEPAY.getLocator())),40);
		pagaconpp=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.PAGA_CON_POSTEPAY.getLocator())),40);
		Utility.swipe(driver, Utility.DIRECTION.DOWN,1000);
		
		scontibancoposta=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.SCONTI_BANCOPOSTA.getLocator())),40);
		sicurezza=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.SICUREZZA.getLocator())),40);
		Utility.swipe(driver, Utility.DIRECTION.DOWN,1000);
		
		ppconnect=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.POSTEPAY_CONNECT.getLocator())),40);
		terminiecondizioni=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.TERMINI_E_CONDIZIONI.getLocator())),40);
		bloccacarte=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.BLOCCA_CARTE.getLocator())),40);
		Utility.swipe(driver, Utility.DIRECTION.DOWN,1000);
		
		aiuto=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.AIUTO.getLocator())),40);
		chiamacibutton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.CHIAMACI_BUTTON.getLocator())),40);
		Utility.swipe(driver, Utility.DIRECTION.DOWN,1000);
		
		nonhaitrovato=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.NON_HAI_TROVATO.getLocator())),40);
		chattabutton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.CHATTA_BUTTON.getLocator())),40);
		Utility.swipe(driver, Utility.DIRECTION.DOWN,1000);
		
		bancopostaapp=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.BANCOPOSTA.getLocator())),40);
		ufficiopostaleapp=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.UFFICIO_POSTALE.getLocator())),40);

		Assert.assertTrue(title!=null);
		Assert.assertTrue(domandefrequenti!=null);
		Assert.assertTrue(accessoapp!=null);
		Assert.assertTrue(operazioniapp!=null);
		Assert.assertTrue(lemiecartepp!=null);
		Assert.assertTrue(p2p!=null);
		Assert.assertTrue(pagaconpp!=null);
		Assert.assertTrue(scontibancoposta!=null);
		Assert.assertTrue(sicurezza!=null);
		Assert.assertTrue(ppconnect!=null);
		Assert.assertTrue(terminiecondizioni!=null);
		Assert.assertTrue(bloccacarte!=null);
		Assert.assertTrue(aiuto!=null);
		Assert.assertTrue(chiamacibutton!=null);
		Assert.assertTrue(nonhaitrovato!=null);
		Assert.assertTrue(chattabutton!=null);
		Assert.assertTrue(bancopostaapp!=null);
		Assert.assertTrue(ufficiopostaleapp!=null);
	}

	public void openChattaConNoiPage(AssistenzaBean assistenzaBean) {
		chattabutton.click();
		numerotelefono=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.NUMERO_TELEFONO.getLocator())),40);
		prenotabutton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.PRENOTA_BUTTON.getLocator())),40);
		Assert.assertTrue(numerotelefono!=null);
		Assert.assertTrue(prenotabutton!=null);
		numerotelefono.sendKeys(assistenzaBean.getNumber());
		prenotabutton.click();
		try {
			avviso=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.AVVISO.getLocator())),10);
			avviso_ok=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.AVVISO_OK.getLocator())),10);
			avviso_ok.click();
		} catch (Exception e) {
			
		}
		
		 try {
				risposta=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.RISPOSTA.getLocator())),30);
				Assert.assertTrue(risposta!=null);
				System.out.println("HA RISPOSTO L'OPERATORE");

		} catch (Exception e) {
			 requested=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.AssistenzaPage.REQUESTED.getLocator())),50);
			 Assert.assertTrue(requested!=null);
			 System.out.println("ATTENZIONE!!! NON HA RISPOSTO L'OPERATORE");

		}

	}
	
}

package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.BonificoSEPA;
import android.Locator.RechargePhone;
import android.Locator.RechargePostepay;
import bean.datatable.BonificoBean;
import bean.datatable.RechargePhoneBean;
import bean.datatable.RechargePostepayBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class BonificoPage extends LoadableComponent<BonificoPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement iban= null;
	private MobileElement confirmCountrydestinationName= null;
	private MobileElement country= null;
	private MobileElement destinationName= null;
	private MobileElement description= null;
	private MobileElement nextBtn= null;
	private MobileElement avviso= null;
	private MobileElement okbutton= null;

	private MobileElement amount= null;

	public BonificoPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		iban=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.IBAN_FIELD_TXT.getLocator())),40);
		country=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.DESTINATION_COUNTRY.getLocator())),40);
		destinationName=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.DESTINATION_NAME.getLocator())),40);
		amount=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.AMOUNT.getLocator())),40);
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.NEXT_BTN.getLocator())),40);
		Assert.assertTrue(iban!=null);
		Assert.assertTrue(country!=null);
		Assert.assertTrue(destinationName!=null);
		Assert.assertTrue(amount!=null);
		Assert.assertTrue(nextBtn!=null);

	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public OperationCompletedPage completeOperation(BonificoBean bonificoBean){
		iban.sendKeys(bonificoBean.getIban());
		driver.hideKeyboard();
		destinationName.sendKeys(bonificoBean.getDestinationName());
		driver.hideKeyboard();
		country.click();
		confirmCountrydestinationName=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.CONFIRM_DESTINATION_COUNTRY.getLocator())),40);
		confirmCountrydestinationName.click();
		amount=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.AMOUNT.getLocator())),40);
		amount.sendKeys(bonificoBean.getAmount());
		driver.hideKeyboard();
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.NEXT_BTN.getLocator())),40);
		nextBtn.click();
		
		try {
			avviso=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.POPUP_AVVISO.getLocator())),25);
			okbutton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.BUTTON_OK.getLocator())),15);
			okbutton.click();
		} catch (Exception e) {
			
		}
//		if(UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//android.widget.TextView[@text='Avviso']")),40).size()>0){
//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='OK']")),40).click();
//		}
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.NEXT_BTN.getLocator())),40);
		nextBtn.click();
		MobileElement posteIdField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.POSTEID_FIELD_TEXT.getLocator())),40);
		posteIdField.sendKeys(bonificoBean.getPosteId());
		driver.hideKeyboard();
		MobileElement confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.CONFIRM_BTN.getLocator())),40);
		confirmBtn.click();
		
		return new OperationCompletedPage(driver).get();
	}
	
	
	
	
	
}

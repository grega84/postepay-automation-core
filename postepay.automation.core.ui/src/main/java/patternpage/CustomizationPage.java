package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator.Login;
import android.Locator.Tutorial;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class CustomizationPage extends LoadableComponent<CustomizationPage>{
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement title=null;
	private MobileElement acceptBtn= null;
	private MobileElement denyBtn= null;
	public CustomizationPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.CustomizationPage.TITLE.getLocator())),40);
		acceptBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.CustomizationPage.ACCEPT_BTN.getLocator())),40);
		denyBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.CustomizationPage.DENY_BTN.getLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(acceptBtn!=null);
		Assert.assertTrue(denyBtn!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	public enum TypeButton{
		ACCEPT("accept"),
		DENY("deny");
		private String nameButton;
		TypeButton(String nameButton) {
			this.nameButton=nameButton;
		}
		public String getNameButton(){
			return nameButton;
		}
	}
	public HomePage goToHomepage(TypeButton typeButton){
		switch (typeButton.getNameButton()) {
		case "accept":
				acceptBtn.click();
			break;
		case "deny":
			denyBtn.click();
		break;

		default:
			break;
		}
		return new HomePage(driver).get();
	}
}

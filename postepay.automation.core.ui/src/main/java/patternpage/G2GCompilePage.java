package patternpage;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import android.Utility;
import bean.datatable.G2gBean;
import bean.datatable.P2pBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class G2GCompilePage extends LoadableComponent<G2GCompilePage> {
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement title=null;
	private MobileElement gigaquantity=null;
	private MobileElement nextbutton=null;
	private MobileElement singleContact=null;
	private MobileElement destinatario=null;
	private MobileElement nextBtn=null;
	private MobileElement logo=null;
	private MobileElement avviso=null;
	private MobileElement descrizioneAvviso=null;
	private MobileElement buttonOK=null;
	
	public G2GCompilePage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {		
		//System.out.println(driver.getPageSource());
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.TITLE.getLocator())),40);
		gigaquantity=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.GIGA_QUANTITY.getLocator())),40);
		
		try {
			System.out.println(Utility.takePartOfScreenByElement(driver, title).getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(title!=null);
		Assert.assertTrue(gigaquantity!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public void searchContact(G2gBean g2gInfo){
		destinatario=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.DESTINATION.getLocator())),40);
		destinatario.sendKeys(g2gInfo.getRecipient());
	}
	
	public void checklogo(G2gBean g2gInfo){
		logo=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.LOGO.getLocator())),40);
	}
	
	
	public void selectContact(G2gBean g2gInfo){
		singleContact=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.SINGLE_CONTACT.getLocatorContact(g2gInfo.getRecipient()))),40);
		singleContact.click();
		MobileElement elem= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='"+g2gInfo.getRecipient()+"']/parent::android.widget.LinearLayout/parent::android.widget.RelativeLayout//android.view.View")));
//		System.out.println(elem.getAttribute("bounds"));
//		System.out.println(elem.getCoordinates().);
		System.out.println(elem.getSize());
	}
	
	public void selectContactNotEnabledToG2G(G2gBean g2gInfo){
		singleContact=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.SINGLE_CONTACT.getLocatorContact(g2gInfo.getRecipient()))),40);
		singleContact.click();
		//MobileElement elem= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='"+g2gInfo.getRecipient()+"']/parent::android.widget.LinearLayout/parent::android.widget.RelativeLayout//android.view.View")));
//		System.out.println(elem.getAttribute("bounds"));
//		System.out.println(elem.getCoordinates().);
	//	System.out.println(elem.getSize());
	}
	
	
	public void goNext(){
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.NEXT_BUTTON.getLocator())),40);
		nextBtn.click();
	}
	
	public void checkContattoNonAbilitato() {
		avviso=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.AVVISO.getLocator())),40);
		descrizioneAvviso=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.DESCRIZIONE_AVVISO.getLocator())),40);
		buttonOK=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPageCompile.OK_BUTTON.getLocator())),40);
		
		Assert.assertTrue(avviso!=null);
		Assert.assertTrue(descrizioneAvviso!=null);
		Assert.assertTrue(buttonOK!=null);
		buttonOK.click();
	}

	
}

package patternpage;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import android.Utility;
import bean.datatable.G2gBean;
import bean.datatable.P2pBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class G2GDetailsGbPage extends LoadableComponent<G2GDetailsGbPage> {
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement title=null;
	private MobileElement gbdisponibili=null;
	private MobileElement labelgbpiano=null;
	private MobileElement labelgbacquistati=null;
	private MobileElement labelgbricevuti=null;
	private MobileElement numerogbpiano=null;
	private MobileElement numerogbacquistati=null;
	private MobileElement numerogbricevuti=null;
	
	public G2GDetailsGbPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {		
		//System.out.println(driver.getPageSource());
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.TITLE.getLocator())),40);
		
		try {
			System.out.println(Utility.takePartOfScreenByElement(driver, title).getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertTrue(title!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public void checkElements() {
		gbdisponibili=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.GB_DISPONIBILI.getLocator())),40);
		labelgbpiano=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.LABEL_GB_PIANO.getLocator())),40);
		labelgbacquistati=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.LABEL_GB_ACQUISTATI.getLocator())),40);
		labelgbricevuti=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.LABEL_GB_RICEVUTI.getLocator())),40);
		numerogbpiano=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.NUMBER_GB_PIANO.getLocator())),40);
		numerogbacquistati=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.NUMBER_GB_ACQUISTATI.getLocator())),40);
		numerogbricevuti=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GDetailsGbPage.NUMBER_GB_RICEVUTI.getLocator())),40);


//		Assert.assertTrue(gbdisponibili!=null);
//		Assert.assertTrue(labelgbpiano!=null);
//		Assert.assertTrue(labelgbacquistati!=null);
//		Assert.assertTrue(labelgbricevuti!=null);
//		Assert.assertTrue(numerogbpiano!=null);
//		Assert.assertTrue(numerogbacquistati!=null);
//		Assert.assertTrue(numerogbricevuti!=null);
		
	}
	

	
}

package patternpage;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import android.Utility;
import bean.datatable.G2gBean;
import bean.datatable.P2pBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class G2GPage extends LoadableComponent<G2GPage> {
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement title=null;
	private MobileElement disponibili=null;
	private MobileElement g2gregalaBtn=null;
	private MobileElement utilizzati=null;
	private MobileElement regala=null;
	private MobileElement vediDettagli=null;
	private MobileElement acquistaGb=null;
	
	
	public G2GPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {		
		//System.out.println(driver.getPageSource());
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPage.TITLE.getLocator())),40);
		disponibili=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPage.GB_DISPONIBILI.getLocator())),40);
		utilizzati=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPage.GB_UTILIZZATI.getLocator())),40);
		regala=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPage.G2G_REGALA.getLocator())),40);
		try {
			System.out.println(Utility.takePartOfScreenByElement(driver, title).getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertTrue(title!=null);
		Assert.assertTrue(disponibili!=null);
		Assert.assertTrue(utilizzati!=null);
		Assert.assertTrue(regala!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public G2GRegalaPage goToG2GRegalaPage(){
		g2gregalaBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.G2GPage.G2G_REGALA.getLocator())));
		g2gregalaBtn.click();
		
		return new G2GRegalaPage(driver).get();
	}
	
	public void openSeeDetails() {
		vediDettagli=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPage.G2G_SEE_DETAILS.getLocator())),40);
		vediDettagli.click();
	}
	
	public void openAquistaPage() {
		acquistaGb=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPage.G2G_ACQUISTA.getLocator())),40);
		acquistaGb.click();
	}

	
}

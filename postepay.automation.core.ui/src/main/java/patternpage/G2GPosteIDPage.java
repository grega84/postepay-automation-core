package patternpage;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import android.Utility;
import bean.datatable.G2gBean;
import bean.datatable.P2pBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class G2GPosteIDPage extends LoadableComponent<G2GPosteIDPage> {
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement title=null;
	private MobileElement posteid=null;
	private MobileElement conferma=null;
	private MobileElement invia=null;
	
	public G2GPosteIDPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {		
		//System.out.println(driver.getPageSource());
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPosteIDPage.TITLE.getLocator())),40);
		posteid=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPosteIDPage.POSTE_ID.getLocator())),40);
		conferma=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPosteIDPage.CONFERMA_BUTTON.getLocator())),40);
		//Assert.assertTrue(procediBtn!=null);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(posteid!=null);
		Assert.assertTrue(conferma!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	public void completeOperation(G2gBean posteId) {
		posteid=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPosteIDPage.POSTE_ID.getLocator())),40);
		posteid.sendKeys(posteId.getPosteId());
		driver.hideKeyboard();
		conferma=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GPosteIDPage.CONFERMA_BUTTON.getLocator())),40);
		conferma.click();
	}


}

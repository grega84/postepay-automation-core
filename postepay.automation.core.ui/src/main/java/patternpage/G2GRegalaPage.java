package patternpage;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import android.Utility;
import bean.datatable.P2pBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class G2GRegalaPage extends LoadableComponent<G2GRegalaPage> {
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement descrizione=null;
	private MobileElement procediBtn=null;
	
	public G2GRegalaPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {		
		//System.out.println(driver.getPageSource());
		descrizione=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GRegalaPage.TEXT_DESCRIPTION.getLocator())),40);
		//Assert.assertTrue(procediBtn!=null);
		Assert.assertTrue(descrizione!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public G2GCompilePage goToG2GCompile(){
		procediBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.G2GRegalaPage.PROCEDI_BUTTON.getLocator())),40);
		procediBtn.click();
		return new G2GCompilePage(driver).get();
	}

	
}

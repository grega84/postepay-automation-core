package patternpage;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.carrotsearch.hppc.CharStack;

import android.Locator;
import android.Locator.ItemDetails;
import android.Locator.MovementList;
import android.Utility;
import android.Utility.DIRECTION;
import bean.datatable.BonificoBean;
import bean.datatable.CheckMovementBean;
import bean.datatable.PostagiroBean;
import bean.datatable.RechargePhoneBean;
import bean.datatable.RechargePostepayBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import net.jpountz.util.Utils;
import test.automation.core.UIUtils;

public class HomePage extends LoadableComponent<HomePage> {
	
	
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement lblTitle=null;
	private MobileElement cardContainer=null;
	private MobileElement sendP2PReceiveBar=null;
	private MobileElement sendBtn=null;
	private MobileElement receiveBtn=null;
	private MobileElement g2gBtn=null;
	private MobileElement p2pBtn=null;
	private MobileElement movementList=null;
	private MobileElement titlenotifiche=null;
	private MobileElement attivanotifichebutton=null;
	private MobileElement backrownotifiche=null;
	private MobileElement titlepersonalizzazione=null;
	private MobileElement accettopersonalizzazione=null;
	private MobileElement titleconnect=null;
	private MobileElement connectsaltabutton=null;
	private MobileElement titlegpay=null;
	private MobileElement nonmostrarepiu=null;
	private MobileElement xclose=null;
	
	
	public HomePage(AppiumDriver<MobileElement> driver) {
		this.driver=driver;
	}
	@Override
	protected void isLoaded() throws Error {
//		lblTitle= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Homepage.TITLE.getLocator())));
//		cardContainer= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Homepage.CARD_CONTAINER_FOCUS.getLocator())));
//		sendP2PReceiveBar=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.ReciveP2pSendBar.ALL_BAR_VISIBLE.getLocator())));
//		sendBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.ReciveP2pSendBar.SEND_BTN.getLocator())));
//		System.out.println(driver.getPageSource());
//		driver.getContext();
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.LinearLayout[@resource-id='posteitaliane.posteapp.apppostepay:id/app_bar_layout']//android.widget.ImageButton")));
//		((AndroidDriver<MobileElement>) driver).pressKeyCode(66);
//		MobileElement a=driver.findElement(By.xpath("//*[@NAF='true']"));//NAF="true"
//		a.click();
		System.out.println(((AndroidDriver<MobileElement>) driver).getContext());
		System.out.println(((AndroidDriver<MobileElement>) driver).getCapabilities());
//		sendBtn=((AndroidDriver<MobileElement>) driver).findElementByAndroidUIAutomator("new UiSelector().resourceId(\"posteitaliane.posteapp.apppostepay:id/bt_send\")");
//		sendBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.LinearLayout[@resource-id='posteitaliane.posteapp.apppostepay:id/bt_send']")));
//		androidUtils
//		try {
//			Thread.sleep(35000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Utility.swipeFromBorders(driver, DIRECTION.LEFT, 300);
//		((AndroidDeviceActionShortcuts) driver).pressKeyCode(AndroidKeyCode.BACK);
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		try {
		//	UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/container_splitted'")),60);
		//	Thread.sleep(2000);
		//	WebElement header=driver.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/container_splitted'"));
		//	org.openqa.selenium.Rectangle rect=header.getRect();
			org.openqa.selenium.Dimension rect = driver.manage().window().getSize();
			Thread.sleep(4000);
			new TouchAction(driver).press(rect.width/2, (int) (rect.height*0.15)).perform();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*check tutorial notifiche*/
/*		try {
			titlenotifiche=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialNotifiche.TITLENOTIFICHE.getLocator())),10);
			attivanotifichebutton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialNotifiche.ATTIVANOTIFICHE.getLocator())),5);
			attivanotifichebutton.click();
			backrownotifiche=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialNotifiche.BACKROWNOTIFICHE.getLocator())),40);
			backrownotifiche.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
*/
		
		/*check tutorial personalizzazione*/
/*		try {
			titlepersonalizzazione=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialPersonalizzazione.TITLEPERSONALIZZAZIONE.getLocator())),10);
			accettopersonalizzazione=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialPersonalizzazione.ACCETTOPERSONALIZZIONE.getLocator())),5);
			accettopersonalizzazione.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
*/
		/*check tutorial connect*/
/*		try {
			titleconnect=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialConnect.TITLECONNECT.getLocator())),10);
			connectsaltabutton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialConnect.SALTABUTTON.getLocator())),5);
			connectsaltabutton.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
*/		
		/*check tutorial gpay*/
/*		try {
			titlegpay=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialGPay.TITLEGPAY.getLocator())),10);
			nonmostrarepiu=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialGPay.CHECKBOXNONMOSTRAREPIU.getLocator())),5);
			xclose=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.TutorialGPay.XCHIUSURA.getLocator())),5);
			nonmostrarepiu.click();
			xclose.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
*/
		sendBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.elementToBeClickable(ByXPath.xpath("//*[@text='PAGA']")),60);
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.elementToBeClickable(ById.id("posteitaliane.posteapp.apppostepay:id/bt_send")),40);

//		receiveBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.ReciveP2pSendBar.RECEIVE_BTN.getLocator())));
		p2pBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.ReciveP2pSendBar.P2P_BTN.getLocator())));

//		Assert.assertTrue(cardContainer!=null);
//		Assert.assertTrue(sendP2PReceiveBar!=null);
		Assert.assertTrue(sendBtn!=null);
//		Assert.assertTrue(receiveBtn!=null);
//		Assert.assertTrue(p2pBtn!=null);
	}
	
	public TutorialP2PPage goToP2PConfiguration(){
		p2pBtn.click();
		return new TutorialP2PPage(driver).get();
	}
	public P2PPage goToP2PPage(){
		p2pBtn.click();
		return new P2PPage(driver).get();
	}
	public G2GPage goToG2GPage(){
		g2gBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Homepage.ICON_G2G.getLocator())));
		g2gBtn.click();
		return new G2GPage(driver).get();
	}
	
	
	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	public void checkLastMovement(CheckMovementBean checkMovementBean){
		MobileElement description=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MovementList.MOVEMENT_LIST_AREA_VISIBLE.getItemDetailsLocator("1", ItemDetails.DESCRIPTION))));
		MobileElement paymentAmount=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MovementList.MOVEMENT_LIST_AREA_VISIBLE.getItemDetailsLocator("1", ItemDetails.PAYMENT))));
		MobileElement paymentDate=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MovementList.MOVEMENT_LIST_AREA_VISIBLE.getItemDetailsLocator("1", ItemDetails.PAYMENT_DATE))));
		System.out.println(UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MovementList.MOVEMENT_LIST_AREA_VISIBLE.getItemDetailsLocator("1", ItemDetails.DESCRIPTION)))));
		String expectedDescription=description.getText();
		String expectedDate=paymentDate.getText();
		String expectedAmount=paymentAmount.getText();
		System.out.println("ACTUAL Amount: "+expectedAmount+"  expected Amount: "+checkMovementBean.getAmount());
		Assert.assertTrue(expectedDescription.contains(checkMovementBean.getOperationName()));
		Assert.assertTrue(expectedDate.contains(checkMovementBean.getOperationDate()));
		Assert.assertTrue(expectedAmount.contains(checkMovementBean.getAmount()));
		
	}
	public void checkLastMovements(List<CheckMovementBean> checkMovementsBean){
		
		int k=0;
		for (int i=0;i<checkMovementsBean.size();i++) {
			k=i+1;
			MobileElement description=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MovementList.MOVEMENT_LIST_AREA_VISIBLE.getItemDetailsLocator(""+k, ItemDetails.DESCRIPTION))));
			MobileElement paymentAmount=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MovementList.MOVEMENT_LIST_AREA_VISIBLE.getItemDetailsLocator(""+k, ItemDetails.PAYMENT))));
			MobileElement paymentDate=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MovementList.MOVEMENT_LIST_AREA_VISIBLE.getItemDetailsLocator(""+k, ItemDetails.PAYMENT_DATE))));
			String expectedDescription=description.getText();
			String expectedDate=paymentDate.getText();
			String expectedAmount=paymentAmount.getText();
			Assert.assertTrue(expectedDescription.contains(checkMovementsBean.get(i).getOperationName()));
			Assert.assertTrue(expectedDate.contains(checkMovementsBean.get(i).getOperationDate()));
			Assert.assertTrue(expectedAmount.contains(checkMovementsBean.get(i).getAmount()));
		}		
	}
	
	
	public OperationCompletedPage sendPostagiro(PostagiroBean postagiro){
		sendBtn.click();
		PopUPSendReceive popUPSendReceive=new PopUPSendReceive(driver).get();
		return popUPSendReceive.postagiro(PopUPSendReceive.SendOperation.POSTAGIRO.getName(), postagiro);
	}
	public OperationCompletedPage sendBonifico(BonificoBean bonifico){
		sendBtn.click();
		PopUPSendReceive popUPSendReceive=new PopUPSendReceive(driver).get();
		return popUPSendReceive.bonifico(PopUPSendReceive.SendOperation.BONIFICO_SEPA.getName(), bonifico);
	}

	public OperationCompletedPage sendRechargePostepay(RechargePostepayBean rechargePostepay){
		sendBtn.click();
		PopUPSendReceive popUPSendReceive=new PopUPSendReceive(driver).get();
		return popUPSendReceive.rechargePostepay(PopUPSendReceive.SendOperation.RICARICA_ALTRA_POSTEPAY.getName(), rechargePostepay);
	}
	public OperationCompletedPage sendRechargePhone(RechargePhoneBean rechargePhone){
		sendBtn.click();
		PopUPSendReceive popUPSendReceive=new PopUPSendReceive(driver).get();
		return popUPSendReceive.rechargePhone(PopUPSendReceive.SendOperation.RICARICA_TELEFONICA.getName(), rechargePhone);
	}
	
}

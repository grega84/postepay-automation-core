package patternpage;

import org.codehaus.groovy.transform.tailrec.GotoRecurHereException;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.Login;
import android.Utility;
import bean.datatable.CredentialBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import test.automation.core.UIUtils;

public class LoginPage extends LoadableComponent<LoginPage> {
	
	public final static String TEXT_TAB_POSTEID="POSTEID";
	public final static String TEXT_TAB_POSTE_IT="POSTE.IT";
	
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement lblTitle=null;
	private MobileElement btnRegistration=null;
	private TabAccessoPosteITPosteID tabAccessPage=null;
	private PosteITSubAccessPage posteItTabPage=null;
	private PosteIDSubAccessPage posteIDTabPage=null;
	private MobileElement nonseitu;
	private MobileElement posteidfield;
	private MobileElement buttonConferma;
	private String driverType;
	
	public HomePage loginFromPosteIt(CredentialBean credentialBean){
		tabAccessPage= new TabAccessoPosteITPosteID(driver).get();
		tabAccessPage.selectTab(TEXT_TAB_POSTE_IT);
		this.isLoaded();
		return this.getPosteItTabPage().loginFromPosteIt(credentialBean).get();
	}
	
	
//	public HomePage loginFromPosteId(CredentialBean credentialBean){
//		if(tabAccessPage.getTabNameSelected().equals("POSTE.IT")){
//			tabAccessPage.
//		}
//		return this.posteIDTabPage().loginFromPosteIt(credentialBean);
//	}
	
	
	public CustomizationPage loginFromPosteItToCustomizationPage(CredentialBean credentialBean){
		return this.getPosteItTabPage().loginFromPosteIToCustomization(credentialBean);
	}
	public PosteITSubAccessPage getPosteItTabPage() {
		
		return posteItTabPage;
	}
	public PosteIDSubAccessPage getPosteIDTabPage() {
		return posteIDTabPage;
	}
	public LoginPage(AppiumDriver<MobileElement> driver) 
	{
		this.driver=driver;
		
		this.driverType=driver.getCapabilities().getCapability("device.os").toString().toLowerCase();
	}
	private MobileElement tabSelected;
	private String tabNameSelected;
	@Override
	protected void isLoaded() throws Error {
		
		try {
			nonseitu=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.InserisciPosteID.NONSEITU.getLocator())),10);
			driver.hideKeyboard();
			posteidfield=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.InserisciPosteID.CODICEPOSTEIDINSERTFIELD.getLocator())),10);
			buttonConferma=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.InserisciPosteID.CONFERMA_BTN.getLocator())),10);
			Assert.assertTrue(nonseitu!=null);
			Assert.assertTrue(posteidfield!=null);
			Assert.assertTrue(buttonConferma!=null);
			driver.hideKeyboard();
			nonseitu.click();
			//posteidfield.sendKeys("prisma");
			//buttonConferma.click();
			
		} catch (Exception e) {
			lblTitle= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.TITLE.getLocator(this.driverType))),40);
			btnRegistration= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.REGISTRATION_BTN.getLocator(driverType))),40);
			tabSelected=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Login.LOCATOR_TAB_SELECTED.getLocator(driverType))),40);
			tabNameSelected=tabSelected.getAttribute("text");
			
			Assert.assertTrue(lblTitle!=null);
			Assert.assertTrue(btnRegistration!=null);
			switch (tabNameSelected) {
				case TEXT_TAB_POSTE_IT:
						posteItTabPage=new PosteITSubAccessPage(driver,driverType).get();
					
						break;
				case TEXT_TAB_POSTEID:
					posteIDTabPage=new PosteIDSubAccessPage(driver,driverType).get();
					break;
				default:
					break;
			}
	
		}
		
		
		
//		switch (tabAccessPage.getTabNameSelected()) {
//		case TabAccessoPosteITPosteID.TEXT_TAB_POSTE_IT:
//			posteItTabPage=(PosteITSubAccessPage) tabAccessPage.returnTabSelected();
//			break;
//		case TabAccessoPosteITPosteID.TEXT_TAB_POSTEID:
//			posteIDTabPage=(PosteIDSubAccessPage) tabAccessPage.returnTabSelected();
//			break;
//		default:
//			break;
//		}
		
	}
	
//	public LoginPage(AppiumDriver<MobileElement> driver,boolean swipeTutorialPage) throws Exception {
//		this.driver= driver;
//		if (swipeTutorialPage){
//			Utility.swipe(driver, Utility.DIRECTION.RIGHT, 300);
//			new Thread().sleep(1000);
//			new TutorialSecondPage(driver).get();
//			new Thread().sleep(1000);
//			Utility.swipe(driver, Utility.DIRECTION.RIGHT, 300);
//			new Thread().sleep(1000);
//			new TutorialThirdPage(driver).get();
//			new Thread().sleep(1000);
//			Utility.swipe(driver, Utility.DIRECTION.RIGHT, 300);
//			new Thread().sleep(1000);
//			new TutorialFourthPage(driver).get();
//			new Thread().sleep(1000);
//			Utility.swipe(driver, Utility.DIRECTION.RIGHT, 300);
//			new Thread().sleep(1000);
//			new TutorialFifthPage(driver).get();
//			new Thread().sleep(1000);
//			Utility.swipe(driver, Utility.DIRECTION.RIGHT, 300);
//			new Thread().sleep(1000);
//			TutorialSixthPage tutorialSixthPage=new TutorialSixthPage(driver).get();
//			new Thread().sleep(1000);
//			tutorialSixthPage.goToNextPage();
//		}
//	}
	
	
	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
}

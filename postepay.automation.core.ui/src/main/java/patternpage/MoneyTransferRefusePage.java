package patternpage;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.MoneyTransferThankYouPage;
import android.Locator.OperationCompleted;
import android.Locator.ThankYouPage;
import android.Locator.Tutorial;
import bean.datatable.ThankYouPageBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class MoneyTransferRefusePage extends LoadableComponent<MoneyTransferRefusePage> {
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement transactionText=null;
	private MobileElement operationText=null;
	private MobileElement closeBtn= null;
	private MobileElement tryAgainBtn= null;
	public MoneyTransferRefusePage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}

	@Override
	protected void isLoaded() throws Error {
		//		driver.navigate().back();
		if(UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//android.widget.ImageView")),40).size()==3){
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageView[2]")),40).click();
		}
		transactionText=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MoneyTransferThankYouPage.TRANSACTION_MESSAGE.getLocator())));
		operationText=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MoneyTransferThankYouPage.DESCRIPTION.getLocator())),40);
		closeBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(MoneyTransferThankYouPage.CLOSE_BTN.getLocator())),40);
		Assert.assertTrue(operationText!=null);
		Assert.assertTrue(closeBtn!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public HomePage closePage(){
		closeBtn.click();
//		if(UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//android.widget.TextView[@text='No, Grazie']")),40).size()==1){
//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='No, Grazie']")),40).click();
//		}
		return new HomePage(driver).get();
	}
	public void checkText(ThankYouPageBean thankYouBean){
		if(!StringUtils.isEmpty(thankYouBean.getTransactionMessage())){
			Assert.assertTrue(transactionText.getText().contains(thankYouBean.getTransactionMessage()));
			
		}
		if(!StringUtils.isEmpty(thankYouBean.getOperationText())){
			Assert.assertTrue(operationText.getText().contains(thankYouBean.getOperationText()));
			
		}
	}



}

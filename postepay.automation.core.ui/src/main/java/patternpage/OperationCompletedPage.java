package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.OperationCompleted;
import android.Locator.Tutorial;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class OperationCompletedPage extends LoadableComponent<OperationCompletedPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement operationText=null;
	private MobileElement closeBtn= null;
	public OperationCompletedPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}

	@Override
	protected void isLoaded() throws Error {
		//		driver.navigate().back();
		if(UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//android.widget.ImageView")),40).size()==3){
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.ImageView[2]")),40).click();
		}
		operationText=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(OperationCompleted.TEXT_OPERATION.getLocator())),40);
		closeBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(OperationCompleted.CLOSE_BTN.getLocator())),40);
		Assert.assertTrue(operationText!=null);
		Assert.assertTrue(closeBtn!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public HomePage closePage(){
		closeBtn.click();
//		if(UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//android.widget.TextView[@text='No, Grazie']")),40).size()==1){
//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='No, Grazie']")),40).click();
//		}
		return new HomePage(driver).get();
	}



}

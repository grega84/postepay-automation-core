package patternpage;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.thoughtworks.selenium.webdriven.commands.KeyEvent;

import android.Locator;
import avro.shaded.com.google.common.collect.ImmutableMap;
import bean.datatable.P2pBean;
import freemarker.core.ReturnInstruction.Return;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class P2PInviaPage extends LoadableComponent<P2PInviaPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement title=null;
	private MobileElement searchContactTxt= null;
	private MobileElement contactTabBtn= null;
	private MobileElement contactList= null;
	private MobileElement singleContact= null;
	private MobileElement nextBtn= null;
	private MobileElement destinationMessageTxt= null;
	private MobileElement sendPayment= null;
	private MobileElement headerDescription= null;
	private MobileElement posteIdField= null;
	private MobileElement confirmBtn= null;
	private MobileElement contactP2P= null;
	private MobileElement modifyDestination= null;

	public P2PInviaPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.TITLE.getLocator())),40);
		searchContactTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_SEARCH.getLocator())),40);
//		contactTabBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_TAB.getLocator())),40);
//		contactList=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_LIST.getLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(searchContactTxt!=null);
//		Assert.assertTrue(contactTabBtn!=null);
//		Assert.assertTrue(contactList!=null);
//		Assert.assertTrue(singleContact!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	public void searchContact(P2pBean p2pInfo){
		searchContactTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_SEARCH.getLocator())),40);
		searchContactTxt.sendKeys(p2pInfo.getRecipient());
	}
	public void selectContact(P2pBean p2pInfo){
		singleContact=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.SINGLE_CONTACT.getLocatorContact(p2pInfo.getRecipient()))),40);
		singleContact.click();
		MobileElement elem= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='"+p2pInfo.getRecipient()+"']/parent::android.widget.LinearLayout/parent::android.widget.RelativeLayout//android.view.View")));
//		System.out.println(elem.getAttribute("bounds"));
//		System.out.println(elem.getCoordinates().);
		System.out.println(elem.getSize());
	}
	
	public void goNext(){
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.NEXT_BTN.getLocator())),40);
		nextBtn.click();
	}
	
	public boolean checkContactDestination(P2pBean p2pInfo){
		MobileElement tmpElement=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.P2PInviaPage.DESTINATION_CONTACT_TO_SEND.getLocator())));
		boolean result=false;

		
		if(tmpElement.getText().equals(p2pInfo.getRecipient())){
			result=true;
		}
		return result;
	}
	public OperationCompletedPage sendPaymentTo(String contactName,String description,String posteIdCode){
		searchContactTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_SEARCH.getLocator())),40);
//		if()
		searchContactTxt.sendKeys(contactName);
//		contactTabBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_TAB.getLocator())),40);
//		contactList=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_LIST.getLocator())),40);
//		driver.hideKeyboard();
//		String contactLocator=android.Locator.P2PInviaPage.SINGLE_CONTACT.getLocatorContact(contactName);
		singleContact=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.SINGLE_CONTACT.getLocatorContact(contactName))),40);
		singleContact.click();
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.NEXT_BTN.getLocator())),40);
		nextBtn.click();
		destinationMessageTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.DESTINATION_MESSAGE.getLocator())),40);
		destinationMessageTxt.sendKeys(description);
//		driver.hideKeyboard();
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONFIRM_DESCRIPTION_BTN.getLocator())),40);
		nextBtn.click();
		sendPayment=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.SEND_PAYMENT.getLocator())),40);
		sendPayment.click();
		headerDescription=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.HEADER_DESCRIPTION.getLocator())),40);
		posteIdField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.POSTEID_FIELD_TEXT.getLocator())),40);
		posteIdField.sendKeys(posteIdCode);
		driver.hideKeyboard();
		confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.CONFIRM_BTN.getLocator())),40);
		confirmBtn.click();
		return new OperationCompletedPage(driver).get();
	}
	
	
	public OperationCompletedPage sendPaymentToContactNotSaved(String contactName,String description,String posteIdCode){
		searchContactTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_SEARCH.getLocator())),40);
//		if()
		searchContactTxt.sendKeys(contactName);
//		contactTabBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_TAB.getLocator())),40);
//		contactList=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_LIST.getLocator())),40);
//		driver.hideKeyboard();
//		String contactLocator=android.Locator.P2PInviaPage.SINGLE_CONTACT.getLocatorContact(contactName);
		searchContactTxt.click();
		searchContactTxt.click();
		searchContactTxt.click();
		driver.tap(1, 1030, 1940, 500);
		//driver.tap(fingers, x, y, duration);
	//	singleContact=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.SINGLE_CONTACT.getLocatorContact(contactName))),40);
	//	singleContact.click();
	//	nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.NEXT_BTN.getLocator())),40);
	//	nextBtn.click();
		destinationMessageTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.DESTINATION_MESSAGE.getLocator())),40);
		destinationMessageTxt.sendKeys(description);
//		driver.hideKeyboard();
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONFIRM_DESCRIPTION_BTN.getLocator())),40);
		nextBtn.click();
		sendPayment=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.SEND_PAYMENT.getLocator())),40);
		sendPayment.click();
		headerDescription=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.HEADER_DESCRIPTION.getLocator())),40);
		posteIdField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.POSTEID_FIELD_TEXT.getLocator())),40);
		posteIdField.sendKeys(posteIdCode);
		driver.hideKeyboard();
		confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.CONFIRM_BTN.getLocator())),40);
		confirmBtn.click();
		return new OperationCompletedPage(driver).get();
	}
	
////	public OperationCompletedPage sendPaymentTo(List<P2pBean> p2pList , String posteIdCode){
//		public void sendPaymentTo(List<P2pBean> p2pList){
//
//		searchContactTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACT_SEARCH.getLocator())),40);
//		
//		searchContactTxt.sendKeys(p2pList.get(0).getRecipient());
//		if(p2pList.get(0).getContactAlreadyExists().equals("No")) {
//			searchContactTxt.click();
////			List enterText = Arrays.asList("text", p2pList.get(0).getRecipient());
////			Map<String, Object> cmd =  ImmutableMap.of("command", "input", "args", enterText);
////			driver.executeScript("mobile: shell", cmd);
////			driver.executeScript("mobile: shell", "cmd" );
//			((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_SEARCH);
//			((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.ENTER);
//			((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_BUTTON_START);
//			((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_INSERT);
//			((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_KATAKANA_HIRAGANA);
////			((AndroidDriver) driver).pressKeyCode(new Keyevent
//		
//
//			//driver.executeScript(null, (Boolean) null�mobile: shell�,);
////			((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
//			//driver.getKeyboard().sendKeys(Keys.RETURN);
////			driver.getKeyboard().pressKey(Keys.ENTER);
////			driver.getKeyboard().pressKey(Keys.RETURN);
//			((AndroidDriver) driver).pressKeyCode(84); 
////			Actions a = new Actions(driver);
////			a.sendKeys(Keys.ENTER).perform();
//			
//		}
//		singleContact=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.SINGLE_CONTACT.getLocatorContact(contactName))),40);
//		singleContact.click();
//		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.NEXT_BTN.getLocator())),40);
//		nextBtn.click();
//		destinationMessageTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.DESTINATION_MESSAGE.getLocator())),40);
//		destinationMessageTxt.sendKeys(description);
//		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONFIRM_DESCRIPTION_BTN.getLocator())),40);
//		nextBtn.click();
//		sendPayment=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.SEND_PAYMENT.getLocator())),40);
//		sendPayment.click();
//		headerDescription=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.HEADER_DESCRIPTION.getLocator())),40);
//		posteIdField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.POSTEID_FIELD_TEXT.getLocator())),40);
//		posteIdField.sendKeys(posteIdCode);
//		confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.CONFIRM_BTN.getLocator())),40);
//		confirmBtn.click();
//		return new OperationCompletedPage(driver).get();
//	}
	
	public void changeContactListFromContactToContactP2P() {
		searchContactTxt.clear();
		contactP2P=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.CONTACTP2P_TAB.getLocator())),40);
		contactP2P.click();
	}
	
	public void tapOnMofifyDestination() {
		modifyDestination=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PInviaPage.MODIFY_DESTINATION_BTN.getLocator())),40);
		modifyDestination.click();
	}
}

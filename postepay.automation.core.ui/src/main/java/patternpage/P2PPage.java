package patternpage;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import android.Utility;
import bean.datatable.P2pBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDeviceActionShortcuts;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class P2PPage extends LoadableComponent<P2PPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement title=null;
	private MobileElement amaountText= null;
	private MobileElement closeBtn= null;
	private MobileElement euroSymbol= null;
	private MobileElement sendBtn= null;
	private MobileElement requestBtn= null;
	private MobileElement disclaimerTxt= null;
	private static final String DISCLAIMER_DESCRIPTION="Puoi inviare fino a 25,00� al giorno senza costi. Al superamento della soglia verranno applicate delle commissioni.";
	
	public P2PPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
//		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PPage.TITLE.getLocator())),40);
//		MobileElement tmpElement=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[contains(@text,'Scopri di pi')]")),30);
//		tmpElement.click();
//		((AndroidDeviceActionShortcuts) driver).pressKeyCode(AndroidKeyCode.ENTER);
		
		System.out.println(driver.getPageSource());
//		new Actions(driver).click();
//		new Actions(driver).sendKeys("\n");
		amaountText=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PPage.AMOUNT_TXT.getLocator())),40);
		disclaimerTxt=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PPage.DISCLAIMER_TXT.getLocator())),40);
		try {
			System.out.println(Utility.takePartOfScreenByElement(driver, amaountText).getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		amaountText=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(ByClassName.tagName("android.widget.EditText")),40);
		//		closeBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PPage.CLOSE_BTN.getLocator())),40);
		euroSymbol=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PPage.EURO_SYMBOL.getLocator())),40);
//		Assert.assertTrue(title!=null);
		Assert.assertTrue(amaountText!=null);
//		Assert.assertTrue(closeBtn!=null);
		Assert.assertTrue(euroSymbol!=null);
//		Assert.assertTrue(sendBtn!=null);
//		Assert.assertTrue(requestBtn!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	public HomePage completeP2pOperation(List<P2pBean> p2pInfo) throws Exception{
		P2PInviaPage p2pInviaPage=goToP2PInvia(p2pInfo.get(0).getAmount());
		OperationCompletedPage operationCompletePage=p2pInviaPage.sendPaymentTo(p2pInfo.get(0).getRecipient(), p2pInfo.get(0).getDescription(), p2pInfo.get(0).getPosteId());
//		p2pInviaPage.sendPaymentTo(p2pInfo);
		ThankYouPage tnkPage=new ThankYouPage(driver).get();
		tnkPage.closeThankYouPage();
		return new HomePage(driver).get();
//		return operationCompletePage.closePage();
	}
	
	public HomePage completeP2pOperationForContactNotSaved(List<P2pBean> p2pInfo) throws Exception{
		P2PInviaPage p2pInviaPage=goToP2PInvia(p2pInfo.get(0).getAmount());
		OperationCompletedPage operationCompletePage=p2pInviaPage.sendPaymentToContactNotSaved(p2pInfo.get(0).getRecipient(), p2pInfo.get(0).getDescription(), p2pInfo.get(0).getPosteId());
//		p2pInviaPage.sendPaymentTo(p2pInfo);
		ThankYouPage tnkPage=new ThankYouPage(driver).get();
		tnkPage.closeThankYouPage();
		return new HomePage(driver).get();
//		return operationCompletePage.closePage();
	}
	
	public P2PInviaPage goToP2PInvia(String valueToSend) throws InterruptedException{
		Thread.sleep(1500);
		amaountText.sendKeys(valueToSend);
//		driver.hideKeyboard();
		sendBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PPage.SEND_BTN.getLocator())),40);
		requestBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.P2PPage.REQUEST_BTN.getLocator())),40);
		sendBtn.click();
		return new P2PInviaPage(driver).get();
	}
	
	public void checkDisclaimer(String disclaimerCheck){
		Assert.assertTrue(disclaimerTxt.getText().equals(DISCLAIMER_DESCRIPTION));
	}
	public P2PInviaPage insertAmount(P2pBean p2pInfo) throws InterruptedException{
		return goToP2PInvia(p2pInfo.getAmount());
	}
	
	public HomePage completeP2pOperationAfterChangeContact(List<P2pBean> p2pInfo) throws Exception{
		P2PInviaPage p2pInviaPage = new P2PInviaPage(driver);
		OperationCompletedPage operationCompletePage=p2pInviaPage.sendPaymentTo(p2pInfo.get(0).getRecipient(), p2pInfo.get(0).getDescription(), p2pInfo.get(0).getPosteId());
//		p2pInviaPage.sendPaymentTo(p2pInfo);
		ThankYouPage tnkPage=new ThankYouPage(driver).get();
		tnkPage.closeThankYouPage();
		return new HomePage(driver).get();
//		return operationCompletePage.closePage();
	}
	
	
}

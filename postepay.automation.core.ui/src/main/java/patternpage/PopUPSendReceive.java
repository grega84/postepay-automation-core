package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator.Login;
import android.Locator.Tutorial;
import bean.datatable.BonificoBean;
import bean.datatable.PostagiroBean;
import bean.datatable.RechargePhoneBean;
import bean.datatable.RechargePostepayBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class PopUPSendReceive extends LoadableComponent<PopUPSendReceive>{
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement title=null;
	private MobileElement scrollableList= null;
	private MobileElement singleItem= null;
	public PopUPSendReceive(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PopUpSendReceive.TITLE_POPUP.getLocator())),40);
		scrollableList=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PopUpSendReceive.SCROLLABLE_LIST.getLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(scrollableList!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	public enum SendOperation{
		RICARICA_ALTRA_POSTEPAY("Ricarica altra Postepay"),
		RICARICA_TELEFONICA("Ricarica telefonica"),
		BONIFICO_SEPA("Bonifico SEPA"),
		POSTAGIRO("Postagiro"),
		BOLLETTINO("Bollettino"),
		CARBURANTE("Carburante"),
		PARKING("Parking"),
		TRASPORTO_URBANO("Trasporto urbano"),
		BIGLIETTO_EXTRAURBANO("Biglietto extraurbano");
		private String name;
		
		public String getName(){
			return name;
		}
		SendOperation(String name){
			this.name=name;
		}
		
		
	}
	
	public OperationCompletedPage rechargePostepay(String operatioName,RechargePostepayBean rechargePostepayBean){
		singleItem=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PopUpSendReceive.SCROLLABLE_LIST.getSingleItem(operatioName))),40);
		OperationCompletedPage tmpOperationCompletedPage=null;
		singleItem.click();
		RechargePostepayPage rechargePostepayPage= new RechargePostepayPage(driver).get();
		tmpOperationCompletedPage= rechargePostepayPage.completeRecharge(rechargePostepayBean);
		return tmpOperationCompletedPage;
	
	
	}
	public OperationCompletedPage rechargePhone(String operatioName,RechargePhoneBean rechargePhoneBean){
		singleItem=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PopUpSendReceive.SCROLLABLE_LIST.getSingleItem(operatioName))),40);
		OperationCompletedPage tmpOperationCompletedPage=null;
		singleItem.click();
		RechargePhonePage rechargePhonePage= new RechargePhonePage(driver).get();
		tmpOperationCompletedPage= rechargePhonePage.completeRecharge(rechargePhoneBean);
		return tmpOperationCompletedPage;
	
	}
	public OperationCompletedPage bonifico(String operatioName,BonificoBean bonifico){
		singleItem=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PopUpSendReceive.SCROLLABLE_LIST.getSingleItem(operatioName))),40);
		OperationCompletedPage tmpOperationCompletedPage=null;
		singleItem.click();
		BonificoPage bonificoPage= new BonificoPage(driver).get();
		tmpOperationCompletedPage= bonificoPage.completeOperation(bonifico);
		return tmpOperationCompletedPage;
	
	}
	public OperationCompletedPage postagiro(String operatioName,PostagiroBean postagiro){
		singleItem=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PopUpSendReceive.SCROLLABLE_LIST.getSingleItem(operatioName))),40);
		OperationCompletedPage tmpOperationCompletedPage=null;
		singleItem.click();
		PostagiroPage postagiroPage= new PostagiroPage(driver).get();
		tmpOperationCompletedPage= postagiroPage.completeOperation(postagiro);
		return tmpOperationCompletedPage;
	
	}
}

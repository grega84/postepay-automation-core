package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.OperationCompleted;
import android.Locator.PopUpAlert;
import android.Locator.Tutorial;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class PopUpAlertPage extends LoadableComponent<PopUpAlertPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement title=null;
	private MobileElement description=null;
	private MobileElement confirmBtn= null;
	public PopUpAlertPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}

	@Override
	protected void isLoaded() throws Error {
		//		driver.navigate().back();
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(PopUpAlert.TITLE_POPUP.getLocator())),40);
		description=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(PopUpAlert.DESCRIPTION_POPUP.getLocator())),40);
		confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(PopUpAlert.CONFIRM_BTN.getLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(description!=null);
		Assert.assertTrue(confirmBtn!=null);



	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public MobileElement getTitle() {
		return title;
	}

	public MobileElement getDescription() {
		return description;
	}

	public MobileElement getConfirmBtn() {
		return confirmBtn;
	}

	



}

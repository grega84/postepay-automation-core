package patternpage;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.BonificoSEPA;
import android.Locator.Postagiro;
import android.Locator.RechargePhone;
import android.Locator.RechargePostepay;
import android.Locator.ThankYouPage_G2G;
import bean.datatable.BonificoBean;
import bean.datatable.PostagiroBean;
import bean.datatable.RechargePhoneBean;
import bean.datatable.RechargePostepayBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class PostagiroPage extends LoadableComponent<PostagiroPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement iban= null;
	private MobileElement confirmCountrydestinationName= null;
	private MobileElement destinationName= null;
	private MobileElement description= null;
	private MobileElement nextBtn= null;
	private MobileElement nograzie= null;
	private MobileElement amount= null;

	public PostagiroPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		iban=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Postagiro.IBAN_FIELD_TXT.getLocator())),40);
		destinationName=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Postagiro.DESTINATION_NAME.getLocator())),40);
		amount=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Postagiro.AMOUNT.getLocator())),40);
		description=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Postagiro.DESCRIPTION_TXT.getLocator())),40);
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Postagiro.NEXT_BTN.getLocator())),40);
		Assert.assertTrue(iban!=null);
		Assert.assertTrue(destinationName!=null);
		Assert.assertTrue(amount!=null);
		Assert.assertTrue(nextBtn!=null);

	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public OperationCompletedPage completeOperation(PostagiroBean postagiroBean){
		iban.sendKeys(postagiroBean.getIban());
		driver.hideKeyboard();
		destinationName.sendKeys(postagiroBean.getDestinationName());
		driver.hideKeyboard();
		amount.sendKeys(postagiroBean.getAmount());
		driver.hideKeyboard();
		description=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Postagiro.DESCRIPTION_TXT.getLocator())),40);
		description.sendKeys(postagiroBean.getDescription());
		driver.hideKeyboard();
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Postagiro.NEXT_BTN.getLocator())),40);
		nextBtn.click();
		
		
		try {
			Thread.sleep(2000);
			nograzie=driver.findElement(By.xpath(Postagiro.BUTTON_OK.getLocator()));
			nograzie.click();
		}
		catch(Exception err) {
			
		}
		
		
		/*if(driver.findElements(By.xpath(Postagiro.POPUP_AVVISO.getLocator())).size()>0) {
			driver.findElement(By.xpath(Postagiro.BUTTON_OK.getLocator())).click();
		}*/
		
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(BonificoSEPA.NEXT_BTN.getLocator())),40);
		nextBtn.click();
		MobileElement posteIdField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.POSTEID_FIELD_TEXT.getLocator())),40);
		posteIdField.sendKeys(postagiroBean.getPosteId());
		driver.hideKeyboard();
		MobileElement confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.CONFIRM_BTN.getLocator())),40);
		confirmBtn.click();
		return new OperationCompletedPage(driver).get();
	}
	
	
	
	
	
}

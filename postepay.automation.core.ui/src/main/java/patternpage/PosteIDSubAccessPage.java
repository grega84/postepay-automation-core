package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import bean.datatable.CredentialBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class PosteIDSubAccessPage extends LoadableComponent<PosteIDSubAccessPage> {
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement tabPosteID;
	private MobileElement userName;
	private MobileElement password;
	private MobileElement accediBtn;
	private MobileElement posteidtab;
	private String driverType;

	public PosteIDSubAccessPage(AppiumDriver<MobileElement> driver, String driverType) {
		this.driver=driver;
		this.driverType=driverType;
	}
	

	@Override
	protected void isLoaded() throws Error {
		userName=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.FIELD_USER_POSTEID.getLocator(driverType))),40);
		password=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.FIELD_PASSWORD_POSTEID.getLocator(driverType))),40);
		accediBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.ACCEDI_BTN_POSTEID.getLocator(driverType))),40);
		Assert.assertTrue(userName!=null);
		Assert.assertTrue(password!=null);
		Assert.assertTrue(accediBtn!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public void openTab() {
		tabPosteID=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.POSTEID_TAB.getLocator(driverType))),40);
		tabPosteID.click();
	}
	
	public HomePage login(CredentialBean credentialBean){
		userName.sendKeys(credentialBean.getUserName());
		password.sendKeys(credentialBean.getPassword());
		accediBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.ACCEDI_BTN.getLocator(driverType))),40);
		accediBtn.click();
		return new HomePage(driver).get();
	}

}

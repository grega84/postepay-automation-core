package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.Homepage;
import bean.datatable.CredentialBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidKeyCode;
import test.automation.core.UIUtils;

public class PosteITSubAccessPage extends LoadableComponent<PosteITSubAccessPage> {
	
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement userName;
	private MobileElement password;
	private MobileElement accediBtn;
	private MobileElement posteit;
	private MobileElement posteidfield;
	private MobileElement buttonConferma;
	private String driverType;

	public PosteITSubAccessPage(AppiumDriver<MobileElement> driver, String driverType) 
	{
		this.driver=driver;
		this.driverType=driverType;
	}
	

	@Override
	protected void isLoaded() throws Error {
		userName= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.FIELD_USER_POSTE_IT.getLocator(driverType))),40);
//		userName= 

		password=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.FIELD_PASSWORD.getLocator(driverType))),40);
		accediBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.ACCEDI_BTN.getLocator(driverType))),40);
		posteit=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.POSTE_IT_TAB.getLocator(driverType))),40);
		Assert.assertTrue(userName!=null);
		Assert.assertTrue(password!=null);
		Assert.assertTrue(accediBtn!=null);
		Assert.assertTrue(posteit!=null);
		
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	public CustomizationPage loginFromPosteIToCustomization(CredentialBean credentialBean){
		userName.sendKeys(credentialBean.getUserName());
		driver.hideKeyboard();
		password.sendKeys(credentialBean.getPassword());
		driver.hideKeyboard();
		driver.hideKeyboard();

	accediBtn.click();
		return new CustomizationPage(driver).get();
	}
	public HomePage loginFromPosteIt(CredentialBean credentialBean){
		if(this.driverType.equals("ios"))
			driver.hideKeyboard();
		//posteit.click();
		userName.sendKeys(credentialBean.getUserName());
		
		if(this.driverType.equals("ios"))
			driver.hideKeyboard();

		password.sendKeys(credentialBean.getPassword());
		
		if(this.driverType.equals("ios"))
			driver.hideKeyboard();
		
		accediBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.Login.ACCEDI_BTN.getLocator(driverType))),40);
		accediBtn.click();
		driver.hideKeyboard();
		posteidfield=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.InserisciPosteID.CODICEPOSTEIDINSERTFIELD.getLocator())),40);
		driver.hideKeyboard();
		buttonConferma=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator.InserisciPosteID.CONFERMA_BTN.getLocator())),40);
		
		posteidfield.sendKeys("prisma");
		buttonConferma.click();
		return new HomePage(driver).get();
	}
}

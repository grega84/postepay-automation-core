package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.RechargePhone;
import android.Locator.RechargePostepay;
import bean.datatable.RechargePhoneBean;
import bean.datatable.RechargePostepayBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class RechargePhonePage extends LoadableComponent<RechargePhonePage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement mobileNumber= null;
	private MobileElement nextBtn= null;
	private MobileElement mobileOperator= null;
	private MobileElement amount= null;

	public RechargePhonePage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		mobileNumber=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePhone.MOBILE_NUMBER.getLocator())),40);
		mobileOperator=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePhone.OPERATOR.getLocator())),40);
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePhone.NEXT_BTN.getLocator())),40);
		amount=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePhone.AMOUNT.getLocator())),40);
		Assert.assertTrue(mobileNumber!=null);
		Assert.assertTrue(mobileOperator!=null);
		Assert.assertTrue(nextBtn!=null);
		Assert.assertTrue(amount!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public OperationCompletedPage completeRecharge(RechargePhoneBean rechargePhoneBean){
		mobileNumber.sendKeys(rechargePhoneBean.getPhoneNumber());
		driver.hideKeyboard();
		mobileOperator.sendKeys(rechargePhoneBean.getPhoneOperator());
		driver.hideKeyboard();
		amount.sendKeys(rechargePhoneBean.getAmount());
		driver.hideKeyboard();
		nextBtn.click();
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePhone.NEXT_BTN.getLocator())),40);
		nextBtn.click();
		MobileElement posteIdField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.POSTEID_FIELD_TEXT.getLocator())),40);
		posteIdField.sendKeys(rechargePhoneBean.getPosteId());
		driver.hideKeyboard();
		MobileElement confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.CONFIRM_BTN.getLocator())),40);
		confirmBtn.click();
		return new OperationCompletedPage(driver).get();
	}
	
	
	
	
	
}

package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.RechargePostepay;
import bean.datatable.RechargePostepayBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class RechargePostepayPage extends LoadableComponent<RechargePostepayPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement cardNumber= null;
	private MobileElement nextBtn= null;
	private MobileElement destinationName= null;
	private MobileElement amount= null;
	private MobileElement descriptionField=null;

	public RechargePostepayPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		cardNumber=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePostepay.CARD_NUMBER.getLocator())),40);
		destinationName=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePostepay.DESTINATION_NAME.getLocator())),40);
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePostepay.NEXT_BTN.getLocator())),40);
		amount=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePostepay.AMOUNT.getLocator())),40);
		descriptionField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePostepay.DESCRIPTION_FIELD.getLocator())),40);
		Assert.assertTrue(cardNumber!=null);
		Assert.assertTrue(destinationName!=null);
		Assert.assertTrue(nextBtn!=null);
		Assert.assertTrue(amount!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public OperationCompletedPage completeRecharge(RechargePostepayBean rechargePostepayBean){
		cardNumber.sendKeys(rechargePostepayBean.getCardNumber());
		driver.hideKeyboard();
		destinationName.sendKeys(rechargePostepayBean.getDestinationName());
		driver.hideKeyboard();
		amount.sendKeys(rechargePostepayBean.getAmount());
		driver.hideKeyboard();
		descriptionField.sendKeys("testautomation");
		driver.hideKeyboard();
		nextBtn.click();
		nextBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(RechargePostepay.NEXT_BTN.getLocator())),60);
		nextBtn.click();
		MobileElement posteIdField=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.POSTEID_FIELD_TEXT.getLocator())),40);
		posteIdField.sendKeys(rechargePostepayBean.getPosteId());
		driver.hideKeyboard();
		MobileElement confirmBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(android.Locator.PosteIdCodeRequiredPage.CONFIRM_BTN.getLocator())),40);
		confirmBtn.click();
		return new OperationCompletedPage(driver).get();
	}
	
	
	
	
	
}

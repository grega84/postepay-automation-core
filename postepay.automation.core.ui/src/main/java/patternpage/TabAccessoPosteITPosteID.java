package patternpage;

import org.assertj.core.api.AssertDelegateTarget;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TabAccessoPosteITPosteID extends LoadableComponent<TabAccessoPosteITPosteID> {
	public final static String TEXT_TAB_POSTEID="POSTEID";
	public final static String TEXT_TAB_POSTE_IT="POSTE.IT";
	public final static String LOCATOR_TAB_SELECTED="//android.widget.LinearLayout/android.widget.TextView[@selected='true']";
	
	private String tabNameSelected=null;
	private MobileElement tabSelected=null;
	private AppiumDriver<MobileElement> driver=null;
	private String driverType;
	public String getTabNameSelected(){
		return tabNameSelected;
	}
	public TabAccessoPosteITPosteID(AppiumDriver<MobileElement> driver){
		this.driver=driver;
		this.driverType=driver.getCapabilities().getCapability("device.os").toString();
	}
//	public LoadableComponent<>
	public LoadableComponent returnTabSelected(){
		LoadableComponent pageToLoad=null;
		tabSelected=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_TAB_SELECTED)));
		tabNameSelected=tabSelected.getAttribute("text");
		switch (tabNameSelected) {
		case TEXT_TAB_POSTE_IT:
			pageToLoad=new PosteITSubAccessPage(driver, driverType).get();
			break;
		case TEXT_TAB_POSTEID:
			pageToLoad=new PosteIDSubAccessPage(driver, driverType).get();
			break;
		default:
			break;
		}
		
		return pageToLoad;
	
	}
	
	
	
	@Override
	protected void isLoaded() throws Error {
		returnTabSelected();
		Assert.assertTrue(tabSelected!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);
	}
	public void selectTab(String tabName){
		MobileElement tmpClickElement=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_TAB_SELECTED+"/parent::android.widget.LinearLayout/android.widget.TextView[@text='"+tabName+"']")));
		tmpClickElement.click();
		
	}
}

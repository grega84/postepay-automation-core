package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.ThankYouPage_2;
import android.Locator.ThankYouPage_G2G;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class ThankYouPage extends LoadableComponent<ThankYouPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement title=null;
	private MobileElement closeBtn= null;
	private MobileElement nograzie= null;

	public ThankYouPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(ThankYouPage_2.TITLE.getLocator())),40);
		Assert.assertTrue(title!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	
	public void closeThankYouPage () {
		
		try {
			Thread.sleep(3500);
			nograzie = (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(ThankYouPage_2.NO_GRAZIE_BUTTON.getLocator())),20);
			nograzie.click();
		}
		catch(Exception err) {
			
		}
		closeBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(ThankYouPage_2.CLOSE_BTN.getLocator())),10);
		closeBtn.click();
		
	}
	
	
}

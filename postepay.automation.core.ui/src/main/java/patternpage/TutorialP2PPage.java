package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.Tutorial;
import android.Locator.TutorialP2P;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TutorialP2PPage extends LoadableComponent<TutorialP2PPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement title=null;
	private MobileElement configurationBtn= null;
	private MobileElement closeBtn= null;

	public TutorialP2PPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		title=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(TutorialP2P.TITLE.getLocator())),40);
		configurationBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(TutorialP2P.CONFIGURA_BTN.getLocator())),40);
		closeBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(TutorialP2P.CLOSE_BTN.getLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(configurationBtn!=null);
		Assert.assertTrue(closeBtn!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public P2PPage goP2pOperation(){
		configurationBtn.click();
		return new P2PPage(driver).get();
	}
	
	
	
}

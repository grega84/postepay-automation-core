package patternpage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Locator;
import android.Locator.Tutorial;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TutorialPage extends LoadableComponent<TutorialPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private MobileElement imgLogo=null;
	private MobileElement btnGoToLogin= null;
	public TutorialPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		imgLogo=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Tutorial.TRAINING_LOGO.getLocator())),60);
		btnGoToLogin=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Tutorial.GO_TO_LOGIN_BTN.getLocator())),40);
		Assert.assertTrue(imgLogo!=null);
		Assert.assertTrue(btnGoToLogin!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public LoginPage goToLogin() throws Exception{
		btnGoToLogin.click();
//		Thread.sleep(2000);
		return new LoginPage(driver).get();
	}
	
	
	
}
